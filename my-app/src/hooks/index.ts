import * as api from '../api/api'
import { useInfiniteQuery, useQuery } from "react-query";

const useAllUsers = () => {
return useQuery("users", api.getAllUsers)
}

export {useAllUsers}