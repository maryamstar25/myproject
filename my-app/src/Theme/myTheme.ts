import { createTheme, responsiveFontSizes } from "@mui/material/styles";

export let theme = createTheme({
  palette: {
    primary: {
      light: "#4fb4be",
      main: "#00848e",
      dark: "#005761",
      contrastText: "#fff",
    },
    secondary: {
      light: "#32cd32",
      main: "#008000",
      dark: "#006400",
      contrastText: "#000",
    },
    text: {
      primary: "#ff5823",
      secondary: "#fff",
    },
  },
});

theme = responsiveFontSizes(theme);
