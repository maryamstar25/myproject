import axios from "axios";
 
let baseURL = 'https://jsonplaceholder.typicode.com/';

export let getUsers = axios.create({
  baseURL: baseURL,
  headers: {
    'Content-Type' : "application/json",
  },
});