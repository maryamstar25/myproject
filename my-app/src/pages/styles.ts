

import { makeStyles } from "@mui/styles";
import { theme } from "../Theme/myTheme";

export const useStyles = makeStyles({
  bgColor: {
    backgroundColor: theme.palette.grey[200],
    height: "100vh",
  },
});
